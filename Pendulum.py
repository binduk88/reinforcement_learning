# An introduction to Q learning
import gym   # for environment
import random
import math
import numpy as np  # for advanced mathematical computation

# initializing the Environment for Inverted Pendulum with cart-Pole
env = gym.make('CartPole-v0')
#  No of Bounds for each discrete state , The zip() function returns an iterator of tuples based on the iterable object.
STATE_BOUNDS = list(zip(env.observation_space.low, env.observation_space.high)) # to save it as tuple
STATE_BOUNDS[1] = [-1, 1]   # high value and Low value


# Defining the environment related constants
# Number of discrete states (bucket) per state dimension
Cont_DIMENSION =(1, 80, 150, 80)  # Position, Velocity , Angle, Angular Velocity
# Number of discrete actions
Pen_ACTION = env.action_space.n  # No of Actions

#  Creating a Q-Table for each state-action pair
q_table = np.zeros(Cont_DIMENSION + (Pen_ACTION,))
print(q_table)
# Learning related constants

alpha = 0.5  # alpha Value or learning rate
PrintValue = True  # for printing
# Defining the simulation related constants
No_of_Episodes = 500
Maximum_T = 200
Total_T = 150
min_explore_rate = 0.05

# helper function for simulation
def get_explore_rate(t):
    return max(min_explore_rate, min(1, 1.0 - math.log10((t+1)/25)))


def get_learning_rate(t):
    return max(alpha, min(0.1, 1.0 - math.log10((t+1)/25)))


# Main function for Simulation
def MainInvertedPendulum():

    # Instantiating the learning related parameters
    learning_rate = get_learning_rate(0)
    explore_rate = get_explore_rate(0)
    discount_factor = 0.99  # It can be between 0 and 1

    num_of_times = 0

    for episode in range(No_of_Episodes):

        # Resetting the environment
        obv = env.reset()

        # state initialization
        state_0 = state_to_row(obv)

        for t in range(Maximum_T):
            env.render()

            # Selecting an action
            action = select_action(state_0, explore_rate)

            # Executing the action
            obv, reward, done, _ = env.step(action)

            # Observing the result
            state = state_to_row(obv)
            best_qValue = np.amax(q_table[state])
            q_table[state_0 + (action,)] += learning_rate*(reward + discount_factor*(best_qValue) - q_table[state_0 + (action,)])
            # Setting up for the next iteration
            state_0 = state

            if done:
               print("Episode %d finished after %f time steps" % (episode, t))
               if t >= Total_T:
                   num_of_times += 1
               else:
                   num_of_times = 0
               break
               # Print data
            if PrintValue:
                   print("\nEpisode = %d" % episode)
                   print("t = %d" % t)
                   print("Action: %d" % action)
                   print("State: %s" % str(state))
                   print("Reward: %f" % reward)
                   print("Learning rate: %f" % learning_rate)
                   print("")

        # Update parameters
        explore_rate = get_explore_rate(episode)
        learning_rate = get_learning_rate(episode)

# For selection of the action


def select_action(state, explore_rate):
    # Select a random action
    if random.random() < explore_rate:
        action = env.action_space.sample()
    # Select the action with the highest q
    else:
        action = np.argmax(q_table[state])
    return action

# for filling up the row

def state_to_row(state):
    bucket_indice = []
    for i in range(len(state)):
        if state[i] <= STATE_BOUNDS[i][0]:
            bucket_index = 0
        elif state[i] >= STATE_BOUNDS[i][1]:
            bucket_index = Cont_DIMENSION[i] - 1
        else:
            # Mapping the state bounds to the bucket array
            dimwidth = STATE_BOUNDS[i][1] - STATE_BOUNDS[i][0]
            offset = (Cont_DIMENSION[i]-1)*STATE_BOUNDS[i][0]/dimwidth
            scaling = (Cont_DIMENSION[i]-1)/dimwidth
            bucket_index = int(round(scaling*state[i] - offset))
        bucket_indice.append(bucket_index)
    return tuple(bucket_indice)

# Main function

if __name__ == "__main__":
    MainInvertedPendulum()
